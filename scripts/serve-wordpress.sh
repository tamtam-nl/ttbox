#!/usr/bin/env bash

mkdir /etc/nginx/ssl 2>/dev/null
openssl genrsa -out "/etc/nginx/ssl/$1.key" 2048 2>/dev/null
openssl req -new -key /etc/nginx/ssl/$1.key -out /etc/nginx/ssl/$1.csr -subj "/CN=$1/O=Vagrant/C=NL" 2>/dev/null
openssl x509 -req -days 365 -in /etc/nginx/ssl/$1.csr -signkey /etc/nginx/ssl/$1.key -out /etc/nginx/ssl/$1.crt 2>/dev/null

block="server {
    listen 80;
    listen 443 ssl;
    server_name $1;
    root \"$2\";

    index index.html index.htm index.php;

    charset utf-8;

      location ~ \..*/.*\.php$ {
        return 403;
      }

      location ~* ^/.well-known/ {
        allow all;
      }

      location ~ (^|/)\. {
        return 403;
      }

      location / {
        try_files \$uri \$uri/ /index.php?\$args;
      }

      location ~ /vendor/.*\.php$ {
        deny all;
        return 404;
      }

      location ~ '\.php$|^/update.php' {
        fastcgi_split_path_info ^(.+?\.php)(|/.*)$;

        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param PATH_INFO \$fastcgi_path_info;
        fastcgi_intercept_errors on;
        fastcgi_pass unix:/run/php/php$3-fpm.sock;
      }

      location ~* \.(?:manifest|appcache|html?|xml|json)$ {
        expires -1;
      }

      location ~* \.(?:rss|atom)$ {
        expires 1h;
        add_header Cache-Control \"public\";
      }

      location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
        expires 1M;
        access_log off;
        add_header Cache-Control \"public\";
      }

      location ~* \.(?:css|js)$ {
        expires 1y;
        access_log off;
        add_header Cache-Control \"public\";
      }

     location /phpmyadmin {
           root /usr/share/;
           index index.php index.html index.htm;
           location ~ ^/phpmyadmin/(.+\.php)$ {
                try_files \$uri =404;
                root /usr/share/;

                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
                fastcgi_index index.php;
                fastcgi_param PATH_INFO \$fastcgi_path_info;
                fastcgi_intercept_errors on;
                fastcgi_pass unix:/run/php/php5-fpm.sock;
           }
           location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
                   root /usr/share/;
           }
    }
    location /phpMyAdmin {
           rewrite ^/* /phpmyadmin last;
    }

    location ~ /\.ht {
        deny all;
    }

    access_log on;
    access_log  /var/log/nginx/$1-access.log combined;
    error_log  /var/log/nginx/$1-error.log error;

    ssl_certificate     /etc/nginx/ssl/$1.crt;
    ssl_certificate_key /etc/nginx/ssl/$1.key;
}
"

echo "$block" > "/etc/nginx/sites-available/$1"
ln -fs "/etc/nginx/sites-available/$1" "/etc/nginx/sites-enabled/$1"
