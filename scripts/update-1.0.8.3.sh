
# Check if it's already installed

if [ -f /home/vagrant/.update-1.0.8.3 ]
then
    exit 0
fi

touch /home/vagrant/.update-1.0.8.3

# Perform apt-get update first
apt-get update -y

# Make dir /tmp/php7/opcache for opcache storage
mkdir -p /tmp/php7/opcache

# Optimize php.ini settings for php 7
echo "Optimize php.ini settings for php 7"
sed -i -r -e 's/opcache.memory_consumption=256M/opcache.memory_consumption=512/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/opcache.interned_strings_buffer=16/opcache.interned_strings_buffer=32/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.huge_code_pages=1/opcache.huge_code_pages=1/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.max_accelerated_files=2000/opcache.max_accelerated_files=8000/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.max_wasted_percentage=5/opcache.max_wasted_percentage=10/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/xdebug.profiler_enable=1/xdebug.profiler_enable=0/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/xdebug.profiler_enable_trigger=1/xdebug.profiler_enable_trigger=0/g' /etc/php/7.0/fpm/php.ini
service php7.0-fpm restart


# Optimize php.ini settings for php 5.6
echo "Optimize php.ini settings for php 5.6"
sed -i -r -e 's/opcache.memory_consumption=256M/opcache.memory_consumption=512/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/opcache.interned_strings_buffer=16/opcache.interned_strings_buffer=32/g' /opt/php-5.6.22/lib/php.ini
sed -i '$ a opcache.huge_code_pages=1' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/;opcache.max_accelerated_files=2000/opcache.max_accelerated_files=8000/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/;opcache.max_wasted_percentage=5/opcache.max_wasted_percentage=10/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/xdebug.profiler_enable=1/xdebug.profiler_enable=0/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/xdebug.profiler_enable_trigger=1/xdebug.profiler_enable_trigger=0/g' /opt/php-5.6.22/lib/php.ini
service php-5.6.22-fpm restart


# Optimize mariadb
echo "Optimize mariadb"
sed -i -r -e 's/max_allowed_packet	= 16M/max_allowed_packet      = 32M/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i -r -e 's/thread_cache_size       = 8/thread_cache_size       = 16/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i -r -e 's/query_cache_size        = 16M/query_cache_size        = 24M/g' /etc/mysql/mariadb.conf.d/50-server.cnf
service mysql restart

# Install wp-cli
echo "Install wp-cli"
cd /home/vagrant/
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp
cd

echo "TTbox finished upgrading to 1.0.8.3"