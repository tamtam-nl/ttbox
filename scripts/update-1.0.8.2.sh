
# Check if it's already installed

if [ -f /home/vagrant/.update-1.0.8.2 ]
then
    exit 0
fi

touch /home/vagrant/.update-1.0.8.2

# Perform apt-get update first
apt-get update -y

# Install zip, gzip, vim
echo "Install zip, gzip, vim"
apt-get install -y zip gzip vim

# Set right php.ini settings for php 7
echo "Set right php.ini settings for php 7"
sed -i -r -e 's/opcache.memory_consumption=128/opcache.memory_consumption=256M/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.interned_strings_buffer=4/opcache.interned_strings_buffer=16/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.revalidate_freq=2/opcache.revalidate_freq=0/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.fast_shutdown=0/opcache.fast_shutdown=1/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.file_cache="\/tmp\/php7\/opcache"/opcache.file_cache="\/home\/vagrant\/.opcache"/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;opcache.file_cache_only=1/opcache.file_cache_only=0/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/opcache.file_cache_consistency_checks=1/opcache.file_cache_consistency_checks=0/g' /etc/php/7.0/fpm/php.ini
sed -i -r -e 's/;sendmail_path =/sendmail_path ="\/usr\/sbin\/mailhog sendmail ttbox@ttbox.dev"/g' /etc/php/7.0/fpm/php.ini

sed -i '$ a xdebug.profiler_enable=1' /etc/php/7.0/fpm/php.ini
sed -i '$ a xdebug.profiler_enable_trigger=1' /etc/php/7.0/fpm/php.ini
sed -i '$ a xdebug.profiler_output_dir=/var/www/profiler-output' /etc/php/7.0/fpm/php.ini

# Update xdebug version for php 5.6
echo "Update xdebug version for php 5.6"
cd /tmp/
wget https://xdebug.org/files/xdebug-2.4.0.tgz
tar -xvzf xdebug-2.4.0.tgz
cd xdebug-2.4.0
/opt/php-5.6.22/bin/phpize
./configure --enable-xdebug --with-php-config=/opt/php-5.6.22/bin/php-config
make
make test
cp modules/xdebug.so /opt/php-5.6.22/lib/php/extensions/no-debug-non-zts-20131226

# Set right php.ini settings for php 5.6
echo "Set right php.ini settings for php 5.6"
sed -i -r -e 's/opcache.memory_consumption=128/opcache.memory_consumption=256M/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/;opcache.interned_strings_buffer=4/opcache.interned_strings_buffer=16/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/;opcache.revalidate_freq=2/opcache.revalidate_freq=0/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/;opcache.fast_shutdown=0/opcache.fast_shutdown=1/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/opcache.file_cache="\/tmp\/php\/5\/opcache-php"/opcache.file_cache="\/home\/vagrant\/.opcache"/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/opcache.file_cache_consistency_checks=1/opcache.file_cache_consistency_checks=0/g' /opt/php-5.6.22/lib/php.ini
sed -i -r -e 's/;sendmail_path =/sendmail_path ="\/usr\/sbin\/mailhog sendmail ttbox@ttbox.dev"/g' /opt/php-5.6.22/lib/php.ini

sed -i '$ a xdebug.profiler_enable=1' /opt/php-5.6.22/lib/php.ini
sed -i '$ a xdebug.profiler_enable_trigger=1' /opt/php-5.6.22/lib/php.ini
sed -i '$ a xdebug.profiler_output_dir=/var/www/profiler-output' /opt/php-5.6.22/lib/php.ini


# Install latest kraftwagen version
echo "Install latest kraftwagen version"
cd /home/vagrant/.drush
git clone "git://github.com/kraftwagen/kraftwagen.git"
drush cc drush
cd

service php7.0-fpm restart
service php-5.6.22-fpm restart
mkdir 	/var/www/profiler-output
echo "TTbox finished upgrading to 1.0.8.2"