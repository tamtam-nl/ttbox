require 'json'
require 'yaml'

VAGRANTFILE_API_VERSION = "2"
confDir = $confDir ||= File.expand_path("~/.ttbox")

ttboxYamlPath = confDir + "/ttbox.yaml"
ttboxJsonPath = confDir + "/ttbox.json"
afterScriptPath = confDir + "/after.sh"
aliasesPath = confDir + "/aliases"

require File.expand_path(File.dirname(__FILE__) + '/scripts/ttbox.rb')

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    if File.exists? aliasesPath then
        config.vm.provision "file", source: aliasesPath, destination: "~/.bash_aliases"
    end

    if File.exists? ttboxYamlPath then
        Ttbox.configure(config, YAML::load(File.read(ttboxYamlPath)))
    elsif File.exists? ttboxJsonPath then
        Ttbox.configure(config, JSON.parse(File.read(ttboxJsonPath)))
    end

    if File.exists? afterScriptPath then
        config.vm.provision "shell", path: afterScriptPath
    end
end
