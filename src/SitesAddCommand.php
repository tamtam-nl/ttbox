<?php

namespace Tamtam\Ttbox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class SitesAddCommand extends HomesteadCommand
{
    protected function configure()
    {
        $this
            ->setName('site:add')
            ->setDescription('Add new site in ttbox environment')
            ->addArgument(
                'hostname',
                InputArgument::REQUIRED,
                'Site hostname'
            )
            ->addArgument(
                'webroot',
                InputArgument::REQUIRED,
                'Webroot directory path'
            )
            ->addArgument(
                'phpversion',
                InputArgument::OPTIONAL,
                'PHP version 5 or 7'
            )
            ->addArgument(
              'type',
              InputArgument::OPTIONAL,
              'For example wordpress'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = self::loadConfig();

        if (!isset($config['sites']) || !is_array($config['sites'])) {
            $config['sites'] = array();
        }

        $site = array(
            'map' => $input->getArgument('hostname'),
            'to' => $input->getArgument('webroot')
        );

        //set correct php version
        $phpversion = $input->getArgument('phpversion');
        if ($phpversion && $phpversion === '5' || $phpversion && $phpversion === '7') {
            $site['php'] = $phpversion;
        }

        //set type
        $type = $input->getArgument('type');
        if ($type && $type === 'wordpress' || $type && $type === 'symfony') {
          $site['type'] = $type;
        }

        $config['sites'][] = $site;

        file_put_contents(
            self::getInstallPath() . "/ttbox.yaml",
            Yaml::dump($config, 3)
        );

        $output->writeln("Site added");
    }
}
