<?php

namespace Tamtam\Ttbox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class DatabaseAddCommand extends HomesteadCommand
{
    protected function configure()
    {
        $this
            ->setName('database:add')
            ->setDescription('Add database to VM')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Database name'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = self::loadConfig();

        if (!isset($config['databases']) || !is_array($config['databases'])) {
            $config['databases'] = array();
        }

        $config['databases'][] = $input->getArgument('name');

        file_put_contents(
            self::getInstallPath() . "/ttbox.yaml",
            Yaml::dump($config, 3)
        );

        $output->writeln("Database added");
    }
}
