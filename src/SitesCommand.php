<?php

namespace Tamtam\Ttbox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SitesCommand extends HomesteadCommand {
  protected function configure() {
    $this
      ->setName('site:show')
      ->setDescription('List sites');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $config = self::loadConfig();

    if( !isset($config['sites']) || empty($config['sites']) ) {
      $output->writeln("No sites found.");

      return;
    }

    $i = 1;
    foreach( $config['sites'] as $key => $site ) {

      if( isset($site['map']) && isset($site['to']) ) {
        $line = $i . ". | Hostname:" . $site['map'] . " (" . $site['to'] . ")";
        if( isset($site['php']) ) {
          $line .= " | PHP:" . $site['php'];
        }
        if( isset($site['type']) ) {
          $line .= " | Type:" . $site['type'];
        }
        $output->writeln($line);
        $i++;
      }
    }
  }
}
