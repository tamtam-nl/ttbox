<?php

namespace Tamtam\Ttbox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DatabaseCommand extends HomesteadCommand
{
    protected function configure()
    {
        $this
            ->setName('database:show')
            ->setDescription('List databases');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = self::loadConfig();

        if (!isset($config['databases']) || empty($config['databases'])) {
            $output->writeln("No databases found.");
            return;
        }

        $i = 1;
        foreach ($config['databases'] as $key => $database) {

            if (isset($database)) {
                $output->writeln($i . ". " . $database);
                $i++;
            }
        }
    }
}
