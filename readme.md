TTbox Virtual Machines
======================

Requirements
------------
* VirtualBox >= 5.0.22 https://www.virtualbox.org/wiki/Downloads
* Vagrant >= [1.9.0](https://releases.hashicorp.com/vagrant/1.9.0/)
* Composer >= 1.0 https://getcomposer.org/download/

# Optional
* dnsmasq https://wiki.deptagency.com/index.php/Osx#dnsmasq

Windows users
-------------
* On Windows, you need to install Git and use the "Git Bash" shell for running commands


Installation
------------
## Run the following commands:
* composer global config repositories.ttbox vcs git@bitbucket.org:tamtam-nl/ttbox.git
* composer global require tamtam/ttbox
## Add ttbox to your PATH for example (normal terminal):
* export PATH="$HOME/.composer/vendor/bin:$PATH"

Usage
-----
After installation you can use ttbox in your terminal, commands will be shown.

## first boot
1. run 'ttbox init' 
2. edit your yaml file with 'ttbox edit' and change in folders 'map: ~/' to your own project folder
3. run 'ttbox up' to start ttbox. Note: if you change something in folders you always need to reload with 'ttbox reload'
4. modify your sites in yaml ('ttbox edit') or use 'ttbox folder:add' command, after modifying use 'ttbox provision' to apply changes.

## Updating from ubuntu 14.x (v0.0.6) to ubuntu 16.04 (> v0.0.7)
```ttbox ssh```

```cd /var/www```

```mysqldump -uroot -proot -v --all-databases > ttboxbackup.sql```

```exit```

```composer global update tamtam/ttbox```

```ttbox update    ```

```ttbox destroy    ```

```ttbox up    ```

```ttbox ssh    ```

```cd /var/www/    ```

```mysql -uroot -proot```    

```source ttboxbackup.sql```

```quit```