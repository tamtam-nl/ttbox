#!/usr/bin/env bash

ttboxRoot=~/.ttbox

mkdir -p "$ttboxRoot"

cp -i src/stubs/ttbox.yaml "$ttboxRoot/ttbox.yaml"
cp -i src/stubs/after.sh "$ttboxRoot/after.sh"
cp -i src/stubs/aliases "$ttboxRoot/aliases"

echo "Homestead initialized!"
